package com.example.mods.application

import android.app.Application
import com.example.mods.db.ModsRoomDatabase
import com.example.mods.repository.ModsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ModsApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { ModsRoomDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { ModsRepository(database.modsDao()) }
}