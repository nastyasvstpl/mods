package com.example.mods.screens.details

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.mods.R
import com.example.mods.application.ModsApplication
import com.example.mods.base.BaseFragment
import com.example.mods.databinding.FragmentDetailsBinding
import com.example.mods.models.ModEntity
import com.example.mods.screens.main.MainActivity
import java.io.*
import android.content.res.AssetFileDescriptor
import android.util.Log

class DetailsFragment : BaseFragment<FragmentDetailsBinding>() {
    private val TAG = "DetailsFragment"
    override val layoutId: Int = R.layout.fragment_details

    private val viewModel: DetailsViewModel by viewModels {
        DetailsViewModelFactory((requireActivity().application as ModsApplication).repository)
    }

    private val args: DetailsFragmentArgs by navArgs()
    private lateinit var mod: ModEntity

    private var filePath = ""
    private var isItDownloaded = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as MainActivity).setButtonHolderInvisible()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mod = args.modEntity
        changeFavourite(mod.isFavorite == 1)

        binding.titleTv.text = mod.name
        binding.descriptionTv.text = mod.description
        if (mod.isNew == 0) {
            binding.newTv.isVisible = false
        } else {
            binding.newTv.isVisible = mod.isNew != 0
        }

        if (mod.isTop == 0) {
            binding.topTv.isVisible = false
        } else {
            binding.topTv.isVisible = mod.isTop != 0
        }

        Glide.with(requireContext())
            .load(Uri.parse("file:///android_asset/" + mod.modPic))
            .centerCrop()
            .into(binding.detailModIc)

        setupListeners()
        setUpSize()
    }

    private fun setUpSize() {
        val fd = requireContext().assets.openFd(mod.modPic) as AssetFileDescriptor
        val l: Long = fd.length / 1024
        binding.fileSize.text = l.toString()+ "KB"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as MainActivity).setButtonHolderVisible()
    }

    private fun setupListeners() {
        with(binding) {
            backButton.setOnClickListener {
                findNavController().popBackStack()
            }

            addFavButton.setOnClickListener {
                if (mod.isFavorite == 1) {
                    changeFavourite(false)
                    mod.isFavorite = 0
                    viewModel.deleteFromDatabase(mod.id)
                } else {
                    changeFavourite(true)
                    mod.isFavorite = 1
                    viewModel.insertToFavorite(mod)
                }
            }


            downloadButton.setOnClickListener {
                if (ContextCompat.checkSelfPermission(
                        requireActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    if (!isItDownloaded) {
                        onDownloadClick()
                    } else {
                        if (isAppInstalledOrNot()) {
                            if (isItDownloaded)
                                installMod()
                        } else {
                            openGooglePlay()
                        }
                    }
                }
                else {
                    askPermission()
            }
            }
        }
    }

    private fun changeFavourite(isFavourite: Boolean) {
        if (isFavourite)
            binding.addFavButton.setImageResource(R.drawable.ic_add_to_fav_button2)
        else
            binding.addFavButton.setImageResource(R.drawable.ic_remove_from_fav_button2)
    }

    private fun changeDownloadState(mState: String) {

        if (mState == "install") {
            binding.downloadTv.text = resources.getText(R.string.install)
        } else {
            binding.downloadTv.text = resources.getText(R.string.downloading)
        }
    }

    private fun onDownloadClick() {
        changeDownloadState("downloading")

        if (filePath.isEmpty())
            copyAsset()

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            isItDownloaded = true

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                changeDownloadState("install")
        }, 2000)
    }

    private fun isAppInstalledOrNot(): Boolean {
        val pm: PackageManager = requireContext().packageManager
        try {
            pm.getPackageInfo("com.mojang.minecraftpe", 0)
            return true
        } catch (e: PackageManager.NameNotFoundException) {

        }
        return false
    }

    private fun openGooglePlay() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=com.mojang.minecraftpe")
                )
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.mojang.minecraftpe")
                )
            )
        }
    }

    private fun askPermission() {
        val permission = ContextCompat.checkSelfPermission(
            requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            permissionsResultCallback.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        } else {
            println("Permission isGranted")
        }
    }

    private val permissionsResultCallback = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {
        when (it) {
            true -> {
                println("Permission has been granted by user")
            }
            false -> {
                Toast.makeText(requireContext(), "Permission denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun copyAsset() {
        val assetManager: AssetManager = requireContext().assets
        val fileName = mod.fileName

        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null
        try {
            inputStream = assetManager.open(fileName)
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path,
                fileName
            )

            filePath = file.absolutePath
            outputStream = FileOutputStream(file)
            copyFile(inputStream, outputStream)
        } catch (e: IOException) {
            Log.d(TAG, "copyAsset: ${e.message}")
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (e: IOException) {
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close()
                } catch (e: IOException) {
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun copyFile(inputStream: InputStream, outputStream: OutputStream) {
        val buffer = ByteArray(1024)
        var len: Int
        while (inputStream.read(buffer).also { len = it } != -1) {
            outputStream.write(buffer, 0, len)
        }
    }

    private fun installMod() {

        val intent = Intent(Intent.ACTION_VIEW)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.type = "file/*"
        intent.data = Uri.parse("minecraft://?import=$filePath")
        requireActivity().startActivity(intent)
    }
}