package com.example.mods.screens.favorites

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mods.R
import com.example.mods.application.ModsApplication
import com.example.mods.base.BaseFragment
import com.example.mods.databinding.FragmentFavoritesBinding
import com.example.mods.models.ModEntity
import com.example.mods.screens.main.MainActivity
import com.example.mods.screens.mods.adapter.ModsAdapter

class FavoritesFragment : BaseFragment<FragmentFavoritesBinding>() {
    override val layoutId: Int = R.layout.fragment_favorites

    private val viewModel: FavoritesViewModel by viewModels {
        FavoritesViewModelFactory((requireActivity().application as ModsApplication).repository)
    }

    private lateinit var adapter: ModsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ModsAdapter(requireContext(), isFavoriteFragment = true)
        binding.favoritesRv.adapter = adapter

        (activity as MainActivity).clickOnMods {
            Navigation.findNavController(
                requireActivity(),
                R.id.nav_host_fragment
            ).navigate(FavoritesFragmentDirections.actionFavoritesFragmentToModsFragment())
        }
        (activity as MainActivity).clickOnFavorites {
        }

        setupFavoritesRecycler()
        initObservables()
    }

    private fun setupFavoritesRecycler() {
        binding.favoritesRv.apply {
            layoutManager = GridLayoutManager(context, 2)
        }
        adapter.onFavoriteClick = { mod: ModEntity ->

            viewModel.deleteFromDatabase(mod.id)
        }
        adapter.onItemClick = {
            val action =
                FavoritesFragmentDirections.actionFavoritesFragmentToDetailsFragment(adapter.items[it])
            findNavController().navigate(action)
        }
    }

    private fun initObservables() {
        viewModel.modsLiveData()
        viewModel.modsLiveData.observe(viewLifecycleOwner) {
            if (it == null) return@observe

            adapter.replace(it.toMutableList())
        }

    }
}
