package com.example.mods.screens.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mods.base.BaseViewModel
import com.example.mods.models.ModEntity
import com.example.mods.repository.ModsRepository
import kotlinx.coroutines.launch

class DetailsViewModel(private val repository: ModsRepository) : BaseViewModel() {

    fun insertToFavorite(mod: ModEntity) {
        launch {
            repository.insertToFavorite(mod)
        }
    }

    fun deleteFromDatabase(id: Int) {
        launch {
            repository.deleteFromDatabase(id)
        }
    }
}

class DetailsViewModelFactory(private val repository: ModsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}