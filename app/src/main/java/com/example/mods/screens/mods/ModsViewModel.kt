package com.example.mods.screens.mods

import android.content.res.AssetManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.mods.base.BaseViewModel
import com.example.mods.models.ModEntity
import com.example.mods.models.ModEntityResponse
import com.example.mods.repository.ModsRepository
import com.google.gson.Gson
import kotlinx.coroutines.launch
import org.json.JSONObject

class ModsViewModel(private val repository: ModsRepository) : BaseViewModel()  {
    val modsLiveData = MutableLiveData<MutableList<ModEntity>>()

    val modsMutableList = mutableListOf<ModEntity>()

    private val TAG = "ModsViewModel"

    fun fetchModsList(assetManager: AssetManager) {

        modsMutableList.clear()
        viewModelScope.launch {
            val text = assetManager.open("content.json").bufferedReader().use { it.readText() }

            val reader = JSONObject(text)
            val jsonList = reader.getJSONObject("qbj421_list")

            val favs = repository.getAllFavoritesMods()
            Log.d(TAG, "fetchModsList: ${favs.size}")

            jsonList.keys().forEach {
                val raw1 = jsonList.get(it).toString()
                val mod = Gson().fromJson(raw1, ModEntityResponse::class.java)
                var doesContain = 0
                for (fav in favs) {
                    if (fav.id == it.toInt()) {
                        doesContain = 1
                        break
                    }
                }
                modsMutableList.add(ModEntity(it.toInt(), mod.name, mod.description, doesContain, mod.fileName, mod.modPic, if (mod.isNew) 1 else 0, if(mod.isTop) 1 else 0))
            }

            modsLiveData.postValue(modsMutableList)
        }
    }

    fun insertToFavorite(mod: ModEntity) {
        launch {
            repository.insertToFavorite(mod)
        }
    }

    fun deleteFromDatabase(id: Int) {
        launch {
            repository.deleteFromDatabase(id)
        }
    }
}
class ModsViewModelFactory(private val repository: ModsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ModsViewModel::class.java)) {
            return ModsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}