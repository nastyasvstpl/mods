package com.example.mods.screens.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import com.example.mods.R
import com.example.mods.base.BaseActivity
import com.example.mods.databinding.ActivitySplashBinding
import com.example.mods.screens.main.MainActivity

class SplashActivity() : BaseActivity<ActivitySplashBinding>() {

    override val layoutId: Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val threeSec = 3000

        val timer = object : CountDownTimer(threeSec.toLong(), 10) {
            override fun onTick(millisUntilFinished: Long) {
                val finishedSeconds = threeSec - millisUntilFinished
                val total = (finishedSeconds.toFloat() / threeSec.toFloat() * 100.0).toInt()
                binding.progressBar.progress = total
                binding.percentTv.text = "$total %"
            }
            override fun onFinish() {
                var intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }.start()
    }
}