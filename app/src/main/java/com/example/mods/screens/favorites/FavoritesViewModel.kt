package com.example.mods.screens.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.mods.base.BaseViewModel
import com.example.mods.models.ModEntity
import com.example.mods.repository.ModsRepository
import kotlinx.coroutines.launch

class FavoritesViewModel (private val repository: ModsRepository) : BaseViewModel(){

    val modsLiveData = MutableLiveData<List<ModEntity>>()

    fun modsLiveData() {
        viewModelScope.launch {
            modsLiveData.postValue(repository.getAllFavoritesMods())
        }
    }

    fun insertToFavorite(mod: ModEntity) {
        launch {
            repository.insertToFavorite(mod)
        }
    }

    fun deleteFromDatabase(id: Int) {
        launch {
            repository.deleteFromDatabase(id)
        }
    }

}
class FavoritesViewModelFactory(private val repository: ModsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoritesViewModel::class.java)) {
            return FavoritesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}