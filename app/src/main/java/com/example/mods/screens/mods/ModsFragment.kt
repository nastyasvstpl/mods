package com.example.mods.screens.mods

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mods.R
import com.example.mods.application.ModsApplication
import com.example.mods.base.BaseFragment
import com.example.mods.databinding.FragmentModsBinding
import com.example.mods.models.ModEntity
import com.example.mods.screens.main.MainActivity
import com.example.mods.screens.mods.adapter.ModsAdapter
import kotlinx.coroutines.launch

class ModsFragment : BaseFragment<FragmentModsBinding>() {

    override val layoutId: Int = R.layout.fragment_mods

    private val TAG = "ModsFragment"

    private val viewModel: ModsViewModel by viewModels {
        ModsViewModelFactory((requireActivity().application as ModsApplication).repository)
    }
    private lateinit var adapter: ModsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
//            if (viewModel.modsMutableList.isEmpty())
                viewModel.fetchModsList(requireContext().assets)
        }

        binding.modsRv.apply {
            layoutManager = GridLayoutManager(context, 2)
        }
        adapter = ModsAdapter(requireContext())
        binding.modsRv.adapter = adapter

        (activity as MainActivity).clickOnFavorites {
            Navigation.findNavController(
                requireActivity(),
                R.id.nav_host_fragment
            ).navigate(ModsFragmentDirections.actionModsFragmentToFavoritesFragment())
        }

        (activity as MainActivity).clickOnMods {
        }

        setupModsRecycler()

        initObservers()
    }

    private fun initObservers() {
        viewModel.modsLiveData.observe(viewLifecycleOwner) {
            if (it == null) return@observe
            Log.d(TAG, "list is not null = ${it.size}")
            adapter.addItems(it)
        }
    }

    private fun setupModsRecycler(){

        adapter.onFavoriteClick = { mod: ModEntity ->
            if (mod.isFavorite == 1) {
                viewModel.insertToFavorite(mod)
            } else {
                viewModel.deleteFromDatabase(mod.id)
            }
        }

        adapter.onItemClick = {

            val action =
                ModsFragmentDirections.actionModsFragmentToDetailsFragment(adapter.items[it])
            findNavController().navigate(action)
        }
    }
}