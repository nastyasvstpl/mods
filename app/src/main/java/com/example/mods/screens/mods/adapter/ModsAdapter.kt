package com.example.mods.screens.mods.adapter

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.example.mods.R
import com.example.mods.base.BaseBindingAdapter
import com.example.mods.databinding.ModItemBinding
import com.example.mods.models.ModEntity

class ModsAdapter(private val context: Context, private val isFavoriteFragment: Boolean = false) :
    BaseBindingAdapter<ModEntity, ModsAdapter.ViewHolder, ModItemBinding>() {
    private val TAG = "ModsAdapter"
    var onFavoriteClick: ((ModEntity) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(initBinding(R.layout.mod_item, parent))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (isInPositionsRange(position)) {
            holder.binding.favButton.setOnClickListener {
                Log.d(TAG, "clicked position: $position")
                Log.d(TAG, "viewHolderPosition: ${holder.adapterPosition}")
                Log.d(TAG, "item size: ${items.size}")
                val oldMod = items[holder.adapterPosition]
                val newMod = oldMod.copy(isFavorite = if (oldMod.isFavorite == 0) 1 else 0)
                Log.d(TAG, "before else statement: ")
                if (isFavoriteFragment) {
                    items.removeAt(holder.adapterPosition)
                    notifyItemRemoved(holder.adapterPosition)
                } else {
                    items[holder.adapterPosition] = newMod
                    notifyItemChanged(holder.adapterPosition)
                }

                onFavoriteClick?.invoke(newMod)
            }
            holder.onBind(items[holder.adapterPosition], holder.adapterPosition)
        }
    }

    inner class ViewHolder(binding: ModItemBinding) :
        BaseBindingViewHolder<ModEntity, ModItemBinding>(binding) {

        override fun onBind(item: ModEntity, position: Int) {

            binding.titleTv.text = item.name

            if (item.isNew == 0) {
                binding.newTv.isVisible = false
            } else {
                binding.newTv.isVisible = item.isNew != 0
            }

            if (item.isTop == 0) {
                binding.topTv.isVisible = false
            } else {
                binding.topTv.isVisible = item.isTop != 0
            }

            Glide.with(context)
                .load(Uri.parse("file:///android_asset/" + item.modPic))
                .centerCrop()
                .into(binding.modIv)


            if (item.isFavorite == 0) {
                binding.favButton.setImageResource(R.drawable.ic_remove_from_fav_button)
            } else {
                binding.favButton.setImageResource(R.drawable.ic_add_to_fav_button)
            }

            binding.executePendingBindings()
        }

    }
}
