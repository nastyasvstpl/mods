package com.example.mods.screens.main

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.mods.R
import com.example.mods.base.BaseActivity
import com.example.mods.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val layoutId: Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun clickOnFavorites(onClick: () -> (Unit)) {

        with(binding) {
            favButton.setOnClickListener {
                favButton.isEnabled = false
                modsButton.isEnabled = true
                bombImage.setImageResource(R.drawable.ic_mods_not_clicked)
                favImage.setImageResource(R.drawable.ic_fav_clicked)
                onClick()
            }
        }
    }

    fun clickOnMods(onClick: () -> (Unit)) {

        with(binding) {
            modsButton.setOnClickListener {
                modsButton.isEnabled = false
                favButton.isEnabled = true
                bombImage.setImageResource(R.drawable.ic_mods_clicked)
                favImage.setImageResource(R.drawable.ic_fav_not_clicked)
                onClick()
            }
        }
    }

    fun setButtonHolderInvisible() {
        binding.holder.isVisible = false
    }

    fun setButtonHolderVisible() {
        binding.holder.isVisible = true
    }
}