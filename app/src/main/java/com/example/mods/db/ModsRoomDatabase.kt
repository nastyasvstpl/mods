package com.example.mods.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.mods.models.ModEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [ModEntity::class], version = 4)
abstract class ModsRoomDatabase : RoomDatabase() {

    abstract fun modsDao(): ModsDao

    companion object {
        @Volatile
        private var INSTANCE: ModsRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): ModsRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ModsRoomDatabase::class.java,
                    "mods_database"
                )
                    .addCallback(ModsDatabaseCallback(scope))
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }

        private class ModsDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.modsDao())
                    }
                }
            }
        }

        suspend fun populateDatabase(modsDao: ModsDao) {

        }
    }
}