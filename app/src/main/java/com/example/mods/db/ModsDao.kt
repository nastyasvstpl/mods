package com.example.mods.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mods.models.ModEntity

@Dao
abstract class ModsDao {

    @Insert
    abstract suspend fun insertToFavorite(mod: ModEntity)

    @Query("DELETE FROM mods_table WHERE id = :id")
    abstract suspend fun deleteFromDatabase(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(mod: List<ModEntity>)

    @Query("select * from mods_table")
    abstract suspend fun getAllMods(): List<ModEntity>

    @Query("select count(*) from mods_table")
    abstract suspend fun count(): Int

    @Query("select * from mods_table")
    abstract fun getAllFavoritesMods(): LiveData<List<ModEntity>>
}