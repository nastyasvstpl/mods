package com.example.mods.repository

import com.example.mods.db.ModsDao
import com.example.mods.models.ModEntity

class ModsRepository(private val modsDao: ModsDao) {

    suspend fun insertToFavorite(mod: ModEntity) = modsDao.insertToFavorite(mod)

    suspend fun deleteFromDatabase(id: Int) = modsDao.deleteFromDatabase(id)

    suspend fun getAllFavoritesMods() = modsDao.getAllMods()

    suspend fun getCount() = modsDao.count()
}