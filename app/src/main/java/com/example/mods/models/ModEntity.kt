package com.example.mods.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "mods_table")

data class ModEntity(
    @PrimaryKey val id: Int,
    val name: String,
    val description: String,
    var isFavorite: Int,
    val fileName: String,
    val modPic: String,
    val isNew: Int,
    val isTop: Int
) : Serializable
