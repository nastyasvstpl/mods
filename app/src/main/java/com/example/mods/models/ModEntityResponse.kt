package com.example.mods.models

import com.google.gson.annotations.SerializedName

data class ModEntityResponse(

    @SerializedName("qbj421f2")
    val modPic: String,

    @SerializedName("qbj421i1")
    val description: String,

    @SerializedName("qbj421t3")
    val fileName: String,

    @SerializedName("qbj421d4")
    val name: String,

    @SerializedName("isNewQUEUE")
    val isNew: Boolean,

    @SerializedName("isTopQUEUE")
    val isTop: Boolean
)